#!/bin/bash
### RIF: https://computingforgeeks.com/run-openldap-server-in-docker-containers/?amp

## fase preliminare 
sudo mkdir -p /srv/slapd/database
sudo mkdir -p /srv/slapd/config
sudo mkdir -p /srv/slapd/certificates
sudo chown -R devops: /srv/slapd

## inserire la risoluzione dell'hostname del servizio in /etc/hosts
sudo echo "172.16.180.177 ldap-dev.sidi.mpi.it" >> /etc/hosts

## creo il container openldap-server
podman create \
  --name openldap-server \
  -p 10389:389 \
  -p 10636:636 \
  --network sidi-net \
  -e LDAP_ORGANISATION=redhat \
  -e LDAP_DOMAIN=redhat.ren \
  -e LDAP_ADMIN_USERNAME=admin \
  -e LDAP_ADMIN_PASSWORD=secret \
  -e LDAP_CONFIG_PASSWORD=secret \
  -e LDAP_BASE_DN=dc=redhat,dc=ren \
  -e LDAP_READONLY_USER=true \
  -e LDAP_READONLY_USER_USERNAME=user-ro \
  -e LDAP_READONLY_USER_PASSWORD=secret \
  -e LDAP_TLS=true \
  -e LDAP_TLS_CA_CRT_FILENAME="ca.crt" \
  -e LDAP_TLS_CRT_FILENAME="ldap-dev.sidi.mpi.it.crt" \
  -e LDAP_TLS_KEY_FILENAME="tls.key" \
  -v /srv/slapd/database:/var/lib/ldap:Z \
  -v /srv/slapd/config:/etc/ldap/slapd.d:Z \
  -v /srv/slapd/certificates:/container/service/slapd/assets/certs:Z \
  docker.io/osixia/openldap:latest

# genera i certificati per openldap-server
./create.sh

sudo cp ca.crt /srv/slapd/certificates/ca.crt
sudo cp tls.crt /srv/slapd/certificates/ldap-dev.sidi.mpi.it.crt
sudo cp tls.key /srv/slapd/certificates/tls.key

# trust custom CA
sudo cp ca.crt /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust extract

# start container
systemctl --user enable --now container-openldap-server.service

# Verifica connessione
ldapsearch -H ldaps://ldap-dev.sidi.mpi.it:10636 -x -D "cn=admin,dc=redhat,dc=ren" -W -b "ou=users,dc=redhat,dc=ren"

# se non funziona:
ldapmodify -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
changetype: modify
replace: olcTLSVerifyClient
olcTLSVerifyClient: allow
EOF

# creo il servizio per openldap-server
podman generate systemd --name openldap-server --files

## creo il contaier phpldapadmin
podman create \
  --name phpldapadmin \
  -p 10080:80 \
  -p 10443:443 \
  --network sidi-net \
  -e PHPLDAPADMIN_LDAP_HOSTS=openldap-server \
  -e PHPLDAPADMIN_HTTPS=true \
  -d docker.io/osixia/phpldapadmin:latest

# creo il servizio per open openldap-server
podman generate systemd --name phpldapadmin --files

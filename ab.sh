#!/bin/bash
# sudo dnf install httpd-tools -y
# usage: ./ab.sh 100 10 https://nginx.apps-crc.testing/
# -n 100: Specifica il numero totale di richieste da eseguire durante il test (in questo caso, 100 richieste).
# -c 10: Imposta il numero di connessioni concorrenti (in questo caso, 10 connessioni).

if [ -z $1 ]; then
        echo "specifica il numero totale di richieste da eseguire"
elif [ -z $2 ]; then
        echo "imposta il numero di connessioni concorrenti"
elif [ -z $3 ]; then
        echo "inserisci una url valida https://..."
else
        ab -n $1 -c $2 $3
fi

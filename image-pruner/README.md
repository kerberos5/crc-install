# Scripts and utilities for installing Codeready Container (CRC)

### Installation steps
``` bash
# Crea in namespace dedicato cleaning
$ oc new-project cleaning

# Crea il serviceaccount image-pruner
$ oc create sa image-pruner

# Fornisci i privilegi necessari al serviceaccount image-pruner
$ oc adm policy add-scc-to-user privileged -z image-pruner
$ oc adm policy add-cluster-role-to-user cluster-admin -z image-pruner

# Crea configmap e Cronjobs
$ oc apply -f ~/crc-install/image-pruner/
$ oc get all
```
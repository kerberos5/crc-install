#!/bin/bash
## fase preliminare 
sudo mkdir -p /srv/gitlab/config
sudo mkdir -p /srv/gitlab/data
sudo mkdir -p /srv/gitlab/logs
sudo mkdir -p /srv/gitlab/certs
sudo cp combined.crt /srv/gitlab/certs/gitlab.sidi.mpi.it.crt
sudo cp tls.key /srv/gitlab/certs/gitlab.sidi.mpi.it.key
sudo chown -R $(whoami): /srv/gitlab
## inserire la risoluzione hostname del servizio in /etc/hosts
sudo echo $(hostname -i) gitlab-dev.sidi.mpi.it >> /etc/hosts
## creo la rete per raggruppare i container
podman network create sidi-net

podman create \
--name gitlab \
--hostname gitlab-dev.sidi.mpi.it \
--network sidi-net \
  -e GITLAB_OMNIBUS_CONFIG="external_url 'https://gitlab.sidi.mpi.it/'; \
  gitlab_rails['initial_root_password'] = 'QGELwIRuajBvpcBQNyOK'; \
  nginx['ssl_certificate'] = '/etc/gitlab/ssl/gitlab.sidi.mpi.it.crt'; \
  nginx['ssl_certificate_key'] = '/etc/gitlab/ssl/gitlab.sidi.mpi.it.key'; \
  gitlab_rails['env'] = { \
    'http_proxy' => 'http://squid.mpi.it:8080/', \
    'https_proxy' => 'http://squid.mpi.it:8080/', \
    'ftp_proxy' => 'http://squid.mpi.it:8080/', \
    'no_proxy' => 'localhost,127.0.0.1,.mpi.it,.sidi.mpi.it,.srv.sogei.it' \
  };" \
-p "8443:443" \
-p "8080:80" \
-p "4422:22" \
--restart always \
--volume /srv/gitlab/config:/etc/gitlab:Z \
--volume /srv/gitlab/logs:/var/log/gitlab:Z \
--volume /srv/gitlab/data:/var/opt/gitlab:Z \
--volume /srv/gitlab/certs:/etc/gitlab/ssl:Z \
-d docker.io/gitlab/gitlab-ce:17.5.5-ce.0

# creo il servizio per openldap-server
podman generate systemd --name gitlab --files
#!/bin/bash
crc stop -f
echo "stopping crc process if exist..."
rm -rf crc-linux-*
echo "remove old binary from filesystem"
cp -R $HOME/crc-linux-* $HOME/crc-version
echo "backup current crc version"
wget https://developers.redhat.com/content-gateway/rest/mirror/pub/openshift-v4/clients/crc/latest/crc-linux-amd64.tar.xz
echo "downalod latest crc version..."
tar xvf crc-linux-amd64.tar.xz
sudo chmod +x crc-linux-*-amd64/crc
sudo rm -f /usr/local/bin/crc
sudo cp crc-linux-*-amd64/crc /usr/local/bin
echo "copy new binary and check components version"
crc version
crc delete -f
echo "removing old istance..."
crc cleanup
echo "cleaning old istance..."
rm -rf ~/.crc
echo "remove old installation from filesystem"
crc config set network-mode user
echo "set network-mode"
crc config set pull-secret-file ~/.mysecrets/pull-secret.txt
echo "set path of pull-secret"
crc config set consent-telemetry yes
echo "enable telemetry"
crc config set memory 24576
echo "set memory to 24GB"
crc config set cpus 8
echo "set cpu to 8"
crc config set disk-size 50
echo "set virtual HD to 50GB"
crc config set http-proxy http://squid.mpi.it:8080
crc config set https-proxy http://squid.mpi.it:8080
crc config set no-proxy .mpi.it,.sidi.mpi.it,.apps-crc.testing,.crc.testing,.apps.ocp4.redhat.ren,.ocp4.redhat.ren
echo "setting proxy directives..."
crc config set enable-cluster-monitoring true
echo "enable the ocp metrics attention!!! high memory consumption"
crc config view
crc setup --log-level debug

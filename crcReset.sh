#!/bin/bash
crc stop -f
echo "stopping crc process if exist..."
crc delete -f
crc cleanup
echo "remove old istance"
crc config set network-mode user
echo "set network-mode"
crc config set pull-secret-file ~/.mysecrets/pull-secret.txt
echo "set path of pull-secret"
crc config set consent-telemetry yes
echo "enable telemetry"
crc config set memory 24576
echo "set memory to 24GB"
crc config set cpus 8
echo "set cpu to 8"
crc config set disk-size 50
echo "set virtual HD to 50GB"
crc config set http-proxy http://squid.mpi.it:8080
crc config set https-proxy http://squid.mpi.it:8080
crc config set no-proxy .mpi.it,.sidi.mpi.it,.apps-crc.testing,.crc.testing,.apps.ocp4.redhat.ren,.ocp4.redhat.ren
echo "setting proxy directives"
crc config set enable-cluster-monitoring true
echo "enable the ocp metrics attention!!! high memory consumption"
crc config view
crc setup --log-level debug

# Configura istanza custom ArgoCD

## Install operator Red Hat OpenShift GitOps
Installa operatore da console OCP esempio 1.10.1 provided by Red Hat Inc

## Creo i gruppi e relativi permessi
``` bash
$ oc adm groups new ocpadmins
$ oc adm groups new ocpdevs

$ oc adm groups add-users ocpadmins admin
$ oc adm groups add-users ocpdevs developer

$ oc adm policy add-cluster-role-to-group cluster-admin ocpadmins
$ oc adm policy add-cluster-role-to-group view ocpdevs
```

## Creo in namespace gitops
``` bash
$ oc new-project gitops
```

## Deploy istanza custom argoCD
``` bash
$ oc create -f ~/crc-install/argocd/argocd.yaml
$ oc get pod -n gitops
$ oc get route -n gitops
```

## Descrizione rbac
``` yaml
rbac:
  defaultPolicy: ""
  policy: |
    g, ocpadmins, role:admin # Concede privilegi amministrativi completi agli utenti appartenenti al gruppo ocpadmins
    g, ocpdevs, role:ocpdevs # Assegna il ruolo ocpdevs agli utenti appartenenti al gruppo ocpdevs
    p, role:ocpdevs, applications, get, */*, allow # Concede permesso di lettura sulle risorse applications a livello globale
    p, role:ocpdevs, projects, get, *, allow # Concede permesso di lettura sulle risorse projects a livello globale
    p, role:ocpdevs, clusters, get, *, allow # Concede permesso di lettura sulle risorse clusters a livello globale
  scopes: '[groups]'
```

